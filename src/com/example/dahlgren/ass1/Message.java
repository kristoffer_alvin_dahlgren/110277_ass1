package com.example.dahlgren.ass1;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/* 
 * 
 * Based on the imt1662/notes 
 */	
public class Message {
	
	long id;
	String message;
	
	private Message() {}
	
	public Message(String message) {
		this.message = message;
	}
	
	// Push a message to database
	public void save(DatabaseHelper dbHelper) {
		final ContentValues values = new ContentValues();
		values.put(MESSAGE, this.message);
		
		final SQLiteDatabase db = dbHelper.getReadableDatabase();
		this.id = db.insert(MESSAGES_TABLE_NAME, null, values);
		db.close();
	}
	
	// Loads the messages from the database newest first
	// Returns the list as a message array
	public static Message[] getAll(final DatabaseHelper dbHelper) {
		 final List<Message> messages = new ArrayList<Message>();
		 final SQLiteDatabase db = dbHelper.getWritableDatabase();
		 final Cursor cursor = db.query(MESSAGES_TABLE_NAME,
				 new String[] { ID, MESSAGE}, null, null, null, null, null);
		 
		 cursor.moveToLast();
		 while (!cursor.isBeforeFirst()) {
			 final Message message = cursorToMessage(cursor);
		     messages.add(message);
		     cursor.moveToPrevious();
		 }
		 cursor.close();
		 return messages.toArray(new Message[messages.size()]);
	}
	
	// Gets a string from db, and returns it as a Message
	public static Message cursorToMessage(Cursor cursor) {
		final Message message = new Message();
		message.setMessage(cursor.getString(cursor.getColumnIndex(MESSAGE)));
		return message;
	}

	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String toString() {
		return this.message;
	}
	
	
	// Table names
	public static final String MESSAGES_TABLE_NAME = "MESSAGES";
	static final String ID = "id";
	static final String MESSAGE = "message";
	
	// Create table
	public static final String MESSAGES_CREATE_TABLE = "CREATE TABLE " + Message.MESSAGES_TABLE_NAME
			+ " (" + Message.ID + " INTEGER PRIMARY KEY," + Message.MESSAGE + " TEXT" + ");";
}
