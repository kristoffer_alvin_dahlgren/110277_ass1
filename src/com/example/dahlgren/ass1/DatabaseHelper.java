package com.example.dahlgren.ass1;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/* 
 * An extension of the SQLiteOpenHelper
 * Creates a table for the messages
 * 
 * Based on the imt1662/notes dbHelper
 */	

class DatabaseHelper extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "message.db";
	public static final int DATABASE_VERSION = 1;

	public DatabaseHelper(Context ctx) {
        super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
    }

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(Message.MESSAGES_CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}
}
