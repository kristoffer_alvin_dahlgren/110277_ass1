package com.example.dahlgren.ass1;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.Menu;
import android.widget.TextView;

/* The timestamp activity
 * Reads random.org, and extracts the timestamp
 * The timestamp is saved onPause, and restored onResume
 * 
 * Based on the imt3662/filedownload main activity
 */

public class Timestamp extends Activity {

	protected final static String WEB_URL = "http://www.random.org/integers/?num=1&min=1&max=100&col=1&base=10&format=html&rnd=new";
	
	// If there is net, catch site. 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_timestamp);
		
		if(isNetworkAvailable()){
			try {
	    		new WebPage().execute(new URL(WEB_URL));
	    	} catch (MalformedURLException e) {
	    		e.printStackTrace();
	    	}
		}
	}
	
	
	// Saves the timestamp when the user leaves the activity
	protected void onPause() {
	    super.onPause();
	    final TextView textField = (TextView) findViewById(R.id.textTimestamp);
	    
	    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
	    SharedPreferences.Editor editor = preferences.edit();
	    editor.putString("timestamp", (String) textField.getText());
	    editor.commit();
	}

	// Restores the timestamp from the previous time this activity was run. 
	protected void onResume() {
	    super.onResume();
	    final TextView textField = (TextView) findViewById(R.id.textTimestamp);
	    
	    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
	    textField.setText(preferences.getString("timestamp", "Timestamp:"));
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.timestamp, menu);
		return true;
	}

	/* Buffers the specified website's data
	 * 
	 * From the imt3662/filedownload example
	 */
	public class WebPage extends AsyncTask<URL, Void, String> {

    	// An AsyncTask to load website in background
		@Override
    	protected String doInBackground(URL... urls) {
            assert urls.length == 1;
            return getSourceCode(urls[0]);
        }
    	
    	// searches for the string "Timestmp:", and returns the position of the first letter. 
    	@Override
        protected void onPostExecute(String result) {
            final TextView textField = (TextView) findViewById(R.id.textTimestamp);
            
            int timestampPosition =  result.indexOf("Timestamp:");
            
            result = result.substring(timestampPosition, timestampPosition+34);
            
            textField.setText(result); 
        }
    	
    	// Buffers the site, and returns it as a string
    	private String getSourceCode(URL url) {
    		int BUFFER_SIZE = 2000;
    	    InputStream in = null;
    	    try {
    	        in = openHttpConnection(url);
    	    } catch (IOException e1) {
    	        e1.printStackTrace();
    	        return "";
    	    }
    	    
    	    InputStreamReader isr = new InputStreamReader(in);
    	    int charRead;
    	    
    	    String returnString = "";
    	    char[] inputBuffer = new char[BUFFER_SIZE];          
    	    try {
    	        while ((charRead = isr.read(inputBuffer))>0)
    	        {
    	            String readString = String.copyValueOf(inputBuffer, 0, charRead);                    
    	            returnString += readString;
    	            inputBuffer = new char[BUFFER_SIZE];
    	        }
    	        in.close();
    	    } catch (IOException e) {
    	        e.printStackTrace();
    	        return "";
    	    }   
    	    return returnString;
    	}
    }

	/* Sets up a Http connection to the specified address 
	 * 
	 * From the imt3662/filedownload example
	 */
	private InputStream openHttpConnection(final URL url) throws IOException {
        InputStream in = null;
        int response = -1;
        final URLConnection conn = url.openConnection();
                 
        if (!(conn instanceof HttpURLConnection)) {                     
            throw new IOException("Not an HTTP connection");
        }
        
        try {
            final HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect(); 

            response = httpConn.getResponseCode();                 
            if (response == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();                                 
            }                     
        } catch (Exception ex) {
        	ex.printStackTrace();            
        }
        return in;     
    }

	
	// Check if there is a internet connection available. 
	public boolean isNetworkAvailable() {
		ConnectivityManager conMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = conMan.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			return true;
		}
		return false;
	}
}

