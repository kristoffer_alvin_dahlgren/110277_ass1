package com.example.dahlgren.ass1;


import android.os.Bundle;
import android.app.ListActivity;
import android.content.Intent;
import android.view.Menu;
import android.widget.ArrayAdapter;
/* The Message List
 * If there is a parameter to the intent from main activity, 
 *  add it to the database.
 * Get messages from the database and add them to the list.
 * 
 * Based on the imt1662/notes main activity
 */
public class MessageList extends ListActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        
        if(message != null) {
        	final Message msg = new Message(message);
        	final DatabaseHelper dbHelper = new DatabaseHelper(this);
        	msg.save(dbHelper);
        }
        
		setContentView(R.layout.activity_message_list);
	
		setListAdapter(new ArrayAdapter<Message>(
				this, android.R.layout.simple_list_item_1, 
				Message.getAll(new DatabaseHelper(this))));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.message_list, menu);
		return true;
	}

}
