package com.example.dahlgren.ass1;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

/* The Main Activity
 * This class contains the buttons for creating new messages,
 * view messages, and go to the timestamp activity
 */

public class MainActivity extends Activity {
	public final static String EXTRA_MESSAGE = "com.example.dahlgren.ass1.MESSAGE";
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }	
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.message_list, menu);
		return true;
	}
    
    
    // Creates an intent to send you and the data to the message list activity
    public void sendMessage(View view){
    	Intent intent = new Intent(this, MessageList.class);
    	EditText editText = (EditText) findViewById(R.id.edit_message);
    	String message = editText.getText().toString();
    	intent.putExtra(EXTRA_MESSAGE, message);
    	startActivity(intent);
    }
    
    // Opens the message list activity
    public void viewMessages(View view){
    	Intent intent = new Intent(this, MessageList.class);
    	startActivity(intent);
    }
    
    // Opens the timestamp activity
    public void getTimestamp(View view){
    	Intent intent = new Intent(this, Timestamp.class);
    	startActivity(intent);
	}
    
}
